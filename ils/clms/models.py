from django.db import models
from django.utils import timezone
from django.urls import reverse
# Create your models here.

class Item_type(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

class Item_category(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

class Catalogue(models.Model):
    type = models.ForeignKey(Item_type, on_delete=models.CASCADE)
    category = models.ForeignKey(Item_category, on_delete=models.CASCADE)
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    edition = models.CharField(max_length=32)
    publisher = models.CharField(max_length=200)
    publish_date = models.DateTimeField(blank=True,null=True)
    is_available = models.BooleanField(default=True)

    def __str__(self):
        return self.title

class Broadcast(models.Model):
    subject = models.CharField(max_length=200)
    text = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subject
