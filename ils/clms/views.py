# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView, View, TemplateView, ListView, DetailView
from django.urls import reverse_lazy
from .models import Catalogue, Broadcast

# Create your views here.

class CatalogueListView(ListView):
    model = Catalogue
    context_object_name = 'catalogues'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library Management System"
        return context

class CatalogueDetailView(DetailView):
    context_object_name = 'catalogue_detail'
    model = Catalogue
    template_name = 'clms/catalogue_detail.html'

class CatalogueCreateView(CreateView):
    model = Catalogue
    fields = ('type', 'category', 'title', 'author', 'publisher', 'edition', 'publish_date')
    success_url = reverse_lazy('clms:catalogue')

class CatalogueUpdateView(UpdateView):
    model = Catalogue
    fields = ('type', 'category', 'title', 'author', 'publisher', 'edition', 'publish_date')
    success_url = reverse_lazy('clms:catalogue')


class CatalogueDeleteView(DeleteView):
    model = Catalogue
    success_url = reverse_lazy('clms:catalogue')

class IndexView(TemplateView):
    # Just set this Class Object Attribute to the template page.
    # template_name = 'app_name/site.html'
    template_name = 'clms/clms.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library Management System"
        return context


class BroadcastListView(ListView):
    model = Broadcast
    context_object_name = 'broadcasts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library Management System"
        return context

class BroadcastCreateView(CreateView):
    model = Broadcast
    fields = ('subject', 'text')
    success_url = reverse_lazy('clms:broadcast')


class BroadcastUpdateView(UpdateView):
    model = Broadcast
    fields = ('subject', 'text')
    success_url = reverse_lazy('clms:broadcast')


class BroadcastDeleteView(DeleteView):
    model = Broadcast
    success_url = reverse_lazy('clms:broadcast')
