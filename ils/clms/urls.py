from django.urls import path
from clms import views
from django.conf.urls import url
app_name = 'clms'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('catalogue/', views.CatalogueListView.as_view(), name='catalogue'),
    url(r'^catalogue/(?P<pk>\d+)/$', views.CatalogueDetailView.as_view(), name='cat_detail'),
    url(r'^catalogue/update/(?P<pk>\d+)/$', views.CatalogueUpdateView.as_view(), name='cat_update'),
    url(r'^catalogue/delete/(?P<pk>\d+)/$', views.CatalogueDeleteView.as_view(), name='cat_delete'),
    path('catalogue/add/', views.CatalogueCreateView.as_view(), name='cat_create'),
    path('broadcast/', views.BroadcastListView.as_view(), name='broadcast'),
    path('broadcast/add/', views.BroadcastCreateView.as_view(), name='broadcast_create'),
    url(r'^broadcast/update/(?P<pk>\d+)/$', views.BroadcastUpdateView.as_view(), name='broadcast_update'),
    url(r'^broadcast/delete/(?P<pk>\d+)/$', views.BroadcastDeleteView.as_view(), name='broadcast_delete'),
]
