from django.contrib import admin
from clms.models import Item_type, Item_category, Catalogue, Broadcast
# Register your models here.
admin.site.register(Item_type)
admin.site.register(Item_category)
admin.site.register(Catalogue)
admin.site.register(Broadcast)
