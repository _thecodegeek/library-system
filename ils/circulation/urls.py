from django.urls import path
from circulation import views
app_name = 'circulation'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('issue/', views.IssueIndexView.as_view(), name='issue'),
    path('return/', views.ReturnIndexView.as_view(), name='return'),
]
