# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView, View, TemplateView, ListView, DetailView
from django.urls import reverse_lazy
# Create your views here.

usertype = "patron"

if usertype == "admin":
    issue = "Issue Book"
else:
    issue = "Borrow Book"


class IndexView(TemplateView):
    template_name = 'circulation/circulation.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library"
        context['issue'] = issue
        context['return'] = "Return Book"
        return context

class IssueIndexView(TemplateView):
    template_name = 'circulation/issue.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library"
        context['issue'] = issue
        context['return'] = "Return Book"
        context['circulate'] = issue
        return context

class ReturnIndexView(TemplateView):
    template_name = 'circulation/return.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library"
        context['issue'] = issue
        context['return'] = "Return Book"
        context['circulate'] = "Return Book"
        return context