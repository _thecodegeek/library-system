from django.db import models
from django.utils import timezone
from django.urls import reverse
from clms.models import Catalogue
# Create your models here.

class Issued(models.Model):
    # title = models.ForeignKey('clms.Catalogue')
    # issue_date = models.DateTimeField(default=timezone.now())
    due_date = models.DateTimeField()
    return_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.return_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
