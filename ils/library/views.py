# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView, View, TemplateView, ListView, DetailView
from clms.models import Broadcast
# Create your views here.


class IndexView(ListView):
    model = Broadcast
    context_object_name = 'broadcasts'
    template_name = 'library/library.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Caleb University's Library"
        return context
